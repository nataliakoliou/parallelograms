import pandas
import random
import math
import sklearn.tree
import sklearn.metrics
import sklearn.model_selection
import pydot
import statistics

print_details = False


# Select a feature set here

# all features
features = ['x1','x2','y1','y2','abs_w','abs_h','rel_w','rel_h','max_dim','min_dim']

# slightly easier task, remove position in the grid which does not matter
#features = ['abs_w','abs_h','rel_w','rel_h','max_dim','min_dim']
# a bit harder, one of the two good features is missing
#features = ['abs_w','abs_h','rel_h','max_dim','min_dim']

# really easy task, only the correct features
#features = ['rel_w','rel_h']

# No matter what you choose, do not change this
df_cols = ['x1','x2','y1','y2','abs_w','abs_h','rel_w','rel_h','max_dim','min_dim','label']

# The max acceptable error in accuracy estimation
# between training and production
error = 0.05


def get_label( o ):
    (x1,y1,x2,y2) = o
    if x2 <= x1: raise ValueError
    if y2 <= y1: raise ValueError
    if x2 - x1 == y2 - y1: return 'square'
    else: return 'long'

def extract_features( o ):
    (x1,y1,x2,y2) = o
    abs_w = x2 - x1
    abs_h = y2 - y1
    rel_w = math.ceil( abs_w/abs_h )
    rel_h = math.ceil( abs_h/abs_w )
    max_dim = max( [abs_w,abs_h] )
    min_dim = min( [abs_w,abs_h] )
    return [abs_w,abs_h,rel_w,rel_h,max_dim,min_dim]

def make_object( x1, y1, max_dim=100 ):
    if max_dim<2: raise ValueError
    lim_x2 = min( [x1+max_dim,99] )
    x2 = random.randint( x1+1, lim_x2 )
    lim_y2 = min( [y1+max_dim,99] )
    y2 = random.randint( y1+1, lim_y2 )
    return ( x1, y1, x2, y2 )

def make_square( x1, y1, max_dim=100 ):
    if max_dim<2: raise ValueError
    lim = min( [max(x1,y1)+max_dim, 99] )
    delta = random.randint( max(x1,y1)+1, lim )
    x2 = x1+delta
    y2 = y1+delta
    return ( x1, y1, x2, y2 )

def make_long( x1, y1, max_dim=100 ):
    if max_dim<2: raise ValueError
    # x1,y1 at the edge, impossible to fit long
    if x1 == 98 and y1 == 98: raise ValueError
    lim_x2 = min( [x1+max_dim,99] )
    lim_y2 = min( [y1+max_dim,99] )
    delta_x = random.randint( x1+1, lim_x2 )
    delta_y = random.randint( y1+1, lim_y2 )
    while delta_x == delta_y:
        delta_x = random.randint( x1+1, lim_x2 )
        delta_y = random.randint( y1+1, lim_y2 )
    x2 = x1+delta_x
    y2 = y1+delta_y
    return ( x1, y1, x2, y2 )


def make_training_dataset( n ):
    data = []
    n_sq = 0
    for i in range(0,n):
        x1 = random.randint( 0, 98 )
        y1 = random.randint( 0, 98 )
        o = make_object( x1, y1, 12 )
        f = extract_features( o )
        (x1,y1,x2,y2) = o
        datapoint = [ x1, y1, x2, y2 ]
        datapoint.extend( f )
        l = get_label( o )
        if l == 'square': n_sq += 1
        datapoint.append( l )
        data.append( datapoint )
    if print_details: print( "Made dataset with {:d}/{:d} squares".format(n_sq,n) )
    return pandas.DataFrame( data, columns=df_cols )


def make_production_dataset( n ):
    data = []
    for i in range(0,math.ceil(n/2)):
        x1 = random.randint( 0, 97 )
        y1 = random.randint( 0, 97 )
        try:
            o = make_long( x1, y1, 72 )
        except:
            # x1,y1 at the edge,
            # impossible to fit long
            x1 = random.randint( 0, 90 )
            y1 = random.randint( 0, 90 )
            o = make_long( x1, y1 )
        data.append( o )
    for i in range(0,math.ceil(n/2)):
        x1 = random.randint( 0, 98 )
        y1 = random.randint( 0, 98 )
        o = make_square( x1, y1, 72 )
        data.append( o )
    dataset = []
    n_sq = 0
    for o in data:
        (x1,y1,x2,y2) = o
        datapoint = [ x1, y1, x2, y2 ]
        f = extract_features( o )
        datapoint.extend( f )
        l = get_label( o )
        if l == 'square': n_sq += 1
        datapoint.append( l )
        dataset.append( datapoint )
    return pandas.DataFrame( dataset, columns=df_cols )


def make_one_experiment( n ):
    if n <20: raise ValueError
    df = make_training_dataset( n )
    X = df[features].values
    y = df.label.values

    # Split into 20 folds and use 19/20 for training
    # and 1/20 for validation. Return the best model
    # among these 20 models as the final outcome.
    kf = sklearn.model_selection.KFold( n_splits=20, shuffle=False, random_state=None )
    tests = []
    for train_index, test_index in kf.split(X):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        clf = sklearn.tree.DecisionTreeClassifier( max_depth=6 )
        # For unlimited tree depth:
        #clf = sklearn.tree.DecisionTreeClassifier()
        clf = clf.fit( X_train, y_train )
        y_pred = clf.predict( X_train )
        acc_train = sklearn.metrics.accuracy_score(y_train, y_pred)
        y_pred = clf.predict( X_test )
        acc_test = sklearn.metrics.accuracy_score(y_test, y_pred)
        tests.append( acc_test )
    # Folding is only used to estimate the accuracy
    # Now use all the data to train the classifier
    # that will be deployed.
    avg_acc = statistics.mean( tests )
    clf = sklearn.tree.DecisionTreeClassifier( max_depth=6 )
    clf = clf.fit( X, y )

    df = make_production_dataset( 1000 )
    X = df[features].values
    y = df.label.values
    pred = clf.predict( X )
    actual_acc = sklearn.metrics.accuracy_score( y, pred )

    if print_details:
        print( "Accuracy: from {:.2f} (training) to {:.2f} (validation) to {:.2f}, {:.2f}".format(avg_acc_train, avg_acc, actual_acc, actual_acc-avg_acc) )
        # This creates a tree.dot file that can then be
        # used to create an image of the decision tree
        sklearn.tree.export_graphviz(clf, "tree.dot", filled=True, rounded=True,
                special_characters=True, feature_names=features, class_names=['long','square'])
        graphs = pydot.graph_from_dot_file( "tree.dot" )
        graphs[0].write_png( "tree.png" )

    return actual_acc-avg_acc

def vc_decision_tree( features ):
    l = len( features )
    done = False
    vc = 0
    while not done:
        vc += 1
        q = math.comb(vc, math.floor(vc/2))
        #print("XX VC: {:d} q: {:d} l: {:d}".format(vc,q,l))
        if q > 2*l: done = True
    return vc-1

def vc_paral():
    return 8*math.log(10,2)-1

def chernov( epsilon, delta, vc ):
    return (-math.log(delta,2)+vc)/2/epsilon/epsilon

def blumer( epsilon, delta, vc ):
    return 1/epsilon*(-4*math.log(delta)+8*vc*math.log(13/epsilon))

def ehrenfeucht( epsilon, delta, vc ):
    a = -math.log(delta)/epsilon
    b = (vc-1)/(32*epsilon)
    return max([a,b])

def calc_delta( examples, eps, vc ):
    l = len( features )
    done = False
    vc = 0
    while not done:
        vc += 1
        q = math.comb(vc, math.floor(vc/2))
        #print("XX VC: {:d} q: {:d} l: {:d}".format(vc,q,l))
        if q > 2*l: done = True
    return vc-1


random.seed( 12 )

for n in [125,250,500,1000,2000,4000]:
    count = 0
    for i in range(0,100):
        if print_details: print("Experiment " + str(i) )
        acc_diff = make_one_experiment( n )
        if acc_diff < -error: count += 1
    error_perc = error*100
    print("With {:d} training examples, accuracy drop greater than {:.0f} percentage points happens at {:d}% of the runs".format(n,error_perc,count) )

    if (count > 0) and (count < 100):

        print("The log-size of the 100x100 parallelograms hypothesis space is {:.2f}. This is also an upper bound for the VC complexity of the hypothesis space".format( vc_paral() ))
        print("")

        b = chernov( error, count/100, vc_paral() )
        print("For this hypothesis space, Chernov limit predicts that at least {:.2f} examples are needed to get accuracy drop not greater than {:.0f} percentage points with {:d}% failure probability".format(b,error_perc,count))
        b = chernov( error, 0.05, vc_paral() )
        print("For comparison, with 5% failure probability this would be {:.0f} examples".format(b))
        print("")

        b = blumer( error, count/100, vc_paral() )
        print("For this hypothesis space, Blumer et al. (1989) predict that at least {:.2f} examples are needed to get accuracy drop not greater than {:.0f} percentage points with {:d}% failure probability".format(b,error_perc,count))

        b = blumer( error, 0.05, vc_paral() )
        print("For comparison, with 5% failure probability this would be {:.0f} examples".format(b))

        print("")
        vc = vc_decision_tree(features)
        print("The VC complexity of training a binary decision tree over {:d} real-valued features is {:d}".format( len(features), vc ))
        b = ehrenfeucht( error, count/100, vc )
        print("For this VC, Ehrenfeucht et al. (1989) predict that at most {:.2f} examples are sufficient to get accuracy drop not greater than {:.0f} percentage points with {:d}% failure probability".format(b,error_perc,count))
        b = ehrenfeucht( error, 0.05, vc )
        print("For comparison, with 5% failure probability this would be {:.2f} examples".format(b))
        print("----------------------------")
